#!/bin/sh
if [ "$1" = "" ] || [ "$2" = "" ] || [ "$3" = "" ] ; then
	echo
	echo "Usage: $0 DBNAME DBUSER DBPASSWORD"
	echo " DBNAME is database name"
	echo " DBUSER is the connecting user (must also be configured for db access)"
	echo " DBPASSWORD is the password user will use to connect"
	echo
	exit 1
fi

DBNAME=$1
DBUSER=$2
DBPASSWORD=$3
FILENAME='phpconnect.php'
if [ -f $FILENAME ] ; then
	echo -n "File $FILENAME already exists.  Are you sure you want to replace it (y/n)? "
	read ANSWER
	if [ "$ANSWER" != "y" ] ; then
		echo "Aborting."
		cd $CWD
		exit
	fi
fi
rm $FILENAME
echo "Writing $FILENAME"
echo "<?php"                              >> $FILENAME
echo "\$hostname=\"localhost\";" >> $FILENAME
echo "\$mysql_login='$DBUSER';" >> $FILENAME
echo "\$mysql_passwd='$DBPASSWORD';" >> $FILENAME
echo "\$db_name='$DBNAME';" >> $FILENAME
echo "\$link = mysql_connect(\$hostname,\$mysql_login,\$mysql_passwd) or die('Could not connect ' . mysql_error());" >> $FILENAME
echo "@mysql_select_db(\$db_name) or die(\"select_db failed\");" >> $FILENAME
echo "?>"                                 >> $FILENAME

echo 
echo "Write $FILENAME complete.  Make sure the following is true:"
echo "  $FILENAME is placed in the site public_html directory"
echo "  $DBNAME database has been created (mysql is CREATE DATABASE $DBNAME)"
echo "  $DBUSER has been granted access (mysql is GRANT ALL ON $DBNAME.* TO $DBUSER@localhost IDENTIFIED BY '$DBPASSWORD')"
echo 

