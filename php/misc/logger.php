<?php
function getLogSettings(&$log_path, &$log_write) {
	$log_path="site.log";
	$log_write = array(
		'QUERY' => false,
		'DEBUG' => true,
		'INFO' => false,
		'ERROR' => true
	);

	if (file_exists('log_settings.php'))
		require_once('log_settings.php');
}

function log_write($type,$str) {
	getLogSettings($log_path, $log_write);
	if (!file_exists($log_path)) {
		reset_log();
	}
	if (array_key_exists($type, $log_write) && !$log_write[$type])
		return;
	$h = fopen($log_path,"a");

	$full_self = $_SERVER['PHP_SELF'];
	$split_self = explode("/",$full_self);
	$only_self="";
	if (count($split_self) > 0)
		$only_self = end($split_self);
	$remote_addr = array_key_exists('REMOTE_ADDR', $_SERVER) ? $_SERVER['REMOTE_ADDR'] : 'unknown';
	$message = date("YmdHis") . " " . sprintf("%-15s",$remote_addr) . " " . sprintf("%-20s",$only_self) . " " . sprintf("%-10s",$type) . " " . $str . "\r\n";
	fwrite($h,$message);
	fclose($h);
}


function parse_debug_backtrace($debug) {
	$str = "";
	$tmp = array();
	foreach ($debug as $step => $details) {
		if (!array_key_exists('file', $details) || !array_key_exists('line', $details))
			break;
		
		$tmp[] = sprintf("%s (%s)", $details['file'], $details['line']);
	}
	$str = implode(" <- ", $tmp);
	return $str;
}

function log_error($str) {
	$str .= sprintf("[%s]", parse_debug_backtrace(debug_backtrace()));
	log_write("ERROR",$str);
}

function log_info($str) {
	log_write("INFO",$str);
}

function log_debug($str) {
	log_write("DEBUG",$str);
}

function log_other($type,$str) {
	log_write($type,$str);
}

function reset_log() {
	getLogSettings($log_path, $log_write);
	
	if (file_exists($log_path))
		unlink($log_path);
	
	$h = fopen($log_path, "a");
	if ($h === false)
		exit;
	fwrite($h,"Site Log created " . date("Y-m-d H:i:s") . "\r\n");
	fclose($h);
}

function getLog() {
	getLogSettings($log_path, $log_write);
	
	if (!file_exists($log_path)) {
		reset_log();
	}
	$contents = file_get_contents($log_path);
	return $contents;	
}
