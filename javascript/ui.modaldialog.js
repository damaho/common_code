// similar to jquery-ui dialog except that modal always went full screen on me - I don't want that
(function($){
	$.widget("ui.modaldialog", {
		options : {
			auto_open : false,
			modalbg : null,
			buttons : {},
			user_elements : {},
			user_data: {},
			position_in_viewport : false,
			button_opts : {}
		},

		setData : function(key, data) {
			this.options.user_data[key] = data;
		},
		
		resetData : function() {
			this.options.user_data = {};
		},
		
		getData : function() {
			return this.options.user_data;
		},

		open : function() {
			$(this.element).parent().append(this.options.modalbg);
			this._setPositions();
			this.options.modalbg.css('display','block');
			$(this.element).css('display','block');
		},

		close : function() {
			this.options.modalbg.css('display','none');
			$(this.element).css('display','none');
		},

		_create : function() {
			this._userelements();
			this._buttons();
			this._modalbg($(this.element).attr('id'));
			if (this.options.auto_open)
				this.open();
			else
				this.close();
		},

		_buttons : function() {
			if ($.isEmptyObject(this.options.buttons))
				return;
			var btn_section = $('<div>'), btn_set = $('<div>'), me = this;
			btn_section.addClass('ui-dialog-buttonpane');
			//btn_section.addClass('ui-widget-content');
			btn_section.addClass('ui-helper-clearfix');
			btn_set.addClass('ui-modaldialog-buttonset');
			$.each(this.options.buttons, function(p_name, p_action) {
				var btn = $('<button>');
				btn.html(p_name);
				btn.button().click(p_action);
				btn_set.append(btn);
				if (p_name in me.options.button_opts) {
					if ('class' in me.options.button_opts[p_name]) {
						btn.addClass(me.options.button_opts[p_name].class);
					}
				}
			});
			btn_section.append(btn_set);
			$(this.element).append(btn_section);
		},

		_userelements : function() {
			if ($.isEmptyObject(this.options.user_elements))
				return;
			var usr_section = $('<div>'),
				me = this;
			usr_section.addClass('ui-dialog-user-elements');
			usr_section.addClass('ui-helper-clearfix');
			$.each(this.options.user_elements, function(key, ele) {
				usr_section.append(ele);
			});
			$(this.element).append(usr_section);
		},

		_modalbg : function(p_id) {
			var div = $('<div>');
			div.attr('id',p_id + '__modalbg');
			div.css({
				backgroundColor : '#000',
				zIndex : 90,
				opacity : .5,
				width: '100%',
				height: '100%',
				position: 'absolute',
				top: '0px',
				left: '0px'
			});

			$(this.element).addClass('ui-dialog-content');
			$(this.element).css({
				position: 'absolute',
				zIndex: 101
			});
			this.options.modalbg = div;
		},
		
		_calculateTop : function(div) {
			var top = 0;
			top = (div.innerHeight() / 2) - ($(this.element).innerHeight() / 2);
			return top;
		},
		
		_setPositions : function() {
			var div = $('#' + $(this.element).attr('id') + '__modalbg');
			if (this.options.position_in_viewport) {
				div = $(window); // TODO - make this smarter by referring to scrolltop
			}
			x = (div.innerWidth() / 2) - ($(this.element).innerWidth() / 2);
			y = this._calculateTop(div); 
			$(this.element).css({
				top : y + 'px',
				left : x + 'px'
			});
		}
	});
})($);
