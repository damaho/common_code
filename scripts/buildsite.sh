#!/bin/sh

if [ "$1" = "" ] || [ "$2" = "" ] ; then
	echo
	echo "Usage: $0 REPO [PHPDEST]"
	echo " REPO is site repository"
	echo " PHPDEST is the public_html destination directory (assume same as REPO name if blank)"
	echo
	if [ "$1" = "" ] ; then
		exit 1
	fi
fi

if [ "$CVSHOME" = "" ] ; then
    CVSHOME=$HOME
fi

CWD=`pwd`
REPODIR=$CVSHOME/$1
PHPLOC=$2
DESTDIR=$CVSHOME/public_html/$2
if [ "$2" = "" ] ; then
	PHPLOC=$1
	DESTDIR=$CVSHOME/public_html/$1
fi
OLDIFS="$IFS"

if [ ! -d $REPODIR ] ; then
	echo "Skip $1 copy because it is not there..."
	if [ "$1" = "common_code" ] ; then
		echo "NOTE: Missing common_code repo means your build will probably not work!"
	fi
	echo
	exit 1
fi

# TODO - copy all if conf is missing?
if [ ! -f $REPODIR/copy.conf ] ; then
	echo "Missing configuration file (copy.conf) in $REPODIR"
	echo
	exit 1
fi

if [ ! "$1" = "common_code" ] ; then
	echo "Copying Common Code first..."
	$0 common_code $PHPLOC
fi



if [ ! -d $DESTDIR ] ; then
	mkdir -p $DESTDIR
fi

mkdir -p $DESTDIR
mkdir -p $DESTDIR/images
mkdir -p $DESTDIR/content

cd $REPODIR

wascopy=0
cat copy.conf | while read -r line
do
	fixedline=$(echo $line | sed 's/\\/\//g')
	IFS=";" read -a arr <<< "$fixedline"
	arrlen=${#arr[@]}
	reposrc=$(echo ${arr[0]} | tr -d "{")
	repodest=""
	if [ $arrlen -gt 1 ] ; then
		repodest="$(echo ${arr[1]} |tr -d "}")/"
	fi
	
	DFROM=$REPODIR/$reposrc
	DTO=$DESTDIR/$repodest
	if [ ! -d $DTO ] ; then
		mkdir -p $DTO
	fi
	#echo "cp $DFROM/* $DTO"
	if [ -d $DFROM ] ; then
		echo "Copying FROM $DFROM/* TO $DTO ..."
		cp $DFROM/* $DTO
		wascopy=1
	else
		echo "Skipping $DFROM copy because it does not exist (wants to go to $DTO)..."
	fi
done

cd $CWD
IFS="$OLDIFS"

echo 
if [ $wascopy -eq 1 ] ; then
	echo "All files copied succesfully."
else
	echo "Nothing was copied to $PHPLOC (could not find any of the source code)."
fi
echo

