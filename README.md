Common Code README
===================

This is the common base for my code.

Branches
--------
master - Represents the stable release branch.
develop - Branched from master.  Represents latest development
feature branches - Branched from develop.  Any feature-specific changes.  Pull request for develop merge when tested

Merging develop changes into master:
-----------------------------------
 1. Create a release branch from master release_xxxxx
 2. Merge develop into release_xxxxx
 3. Stage release_xxxxx
 4. Merge release_xxxxx into master and develop

Publishing
----------
Build script can be found in the /scripts directory: buildsite.sh.  Usage:

buildsite <repository-name> [destination]
  repository name - self explanitory
  the destination directory under the public html directory - defaults to same as repo name
  
  Copies the files defined in a copy.conf file to a directory under the public html directory
  
Starting Points (files to become familiar with)
---------------------------------
php/misc/rpccall.php - Pretty much all ajax calls go through rpccall.php
php/phplibs/DataObjects.php (DataObject class) - class to handle any database transactions
php/phplibs/PageContent.php (PageContent class) - All the html output is based on this (almost all elements and layouts extend from PageContent).  I would start here if you're using this.

Known Issues (TODO)
-------------------
- At some point I build Tabber tabs into PageContent - that should go away
- There's a lot of code in here that's not used anymore.  Could use cleanup

- Form handling is pretty bad

This is a homegrown framework - it's not fancy and need quite a bit of polish.  I'll probably migrate to some common framework at some point instead of using my own.

  

   
